# Fromthepage Fork

This is a soft fork of (https://github.com/benwbrum/fromthepage). Modifications to fromthepage in this repo are made in such a way that future rebases will be easy and mostly conflict-free.

Upstream Fromthepage does development on `development` branch. Stable releases are on `main` branch.

This repo has a `fork-main` branch where the fork development happen. When needed, this branch will be rebased on top of `upstream/main` branch.
