module Emldict::Dictpress
  class Entry < ApplicationRecord
    has_many :relations, foreign_key: :from_id
    has_many :definitions, through: :relations, source: :to_entry
  end
end
