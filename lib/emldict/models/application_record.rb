module Emldict::Dictpress
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true

    connects_to database: { writing: :dictpress, reading: :dictpress }
  end
end
