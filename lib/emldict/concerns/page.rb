module Emldict
  # READ BEFORE PROCEEDING
  # ----------------------
  # emldict has two type of entries: main_entry, sub_entry
  # main_entry = The main word. Example: Abandon, Anarchy
  # sub_entry = Related words defined in the same line as the main word. Example: Abandoned, Abandonment
  # Both main_entry and sub_entry have the same structure. The point
  # of having a sub_entry is to relate it with the main_entry just like how the dictionary is written
  #
  # dictpress has two type of entries: main_entry, definition_entry
  # main entry and definition entry resides in the same table "entries"
  # They are connected via table "relations"
  #
  # This is how it will look in dictpress:
  # main_entry = Abandon
  #   type = noun
  #   definition_entry[0] = "give up"
  #   definition_entry[1] = "desert"
  #   definition_entry[2] = "renounce"
  # main_entry = Abandoned (Abandoned will be a sub_entry of Abandon in emldict)
  #   type = adjective
  #   definition_entry[0] = "depraved"
  #   definition_entry[1] = "forsaken"
  #   definition_entry[2] = "profligate"
  # main_entry = Abandonment (Abandonment will be a sub_entry of Abandon in emldict)
  #   definition_entry[0] = "giving up"
  #   definition_entry[1] = "act of abandoning"

  module PageConcern
    extend ActiveSupport::Concern

    included do
      after_update_commit :start_worker_for_remaking_entries_in_dictpress!

      def contents
        source_text || ""
      end

      # Sometimes a page will be a continuation of last entry of the
      # previous page, if so combine both and give result
      def contents_stiched
        @_contents_stiched ||= begin
          contents, emlrefs = contents_stiched_with_emlrefs_private
          contents
        end
      end

      def contents_stiched_with_emlrefs
        @_contents_stiched ||= contents_stiched_with_emlrefs_private
      end

      def emlref
        "#{work.slug}/page/#{position}"
      end

      def contents_with_emlref
        "set_page_emlref(\"#{emlref}\")\n#{contents}"
      end

      def remake_entries_in_dictpress
        delete_entries_in_dictpress
        add_entries_in_dictpress_with_csv
      end
    end

    private

      def start_worker_for_remaking_entries_in_dictpress!
        rake_call = "RAILS_ENV=#{ENV['RAILS_ENV']} #{RAKE} emldict:remake_entries_in_dictpress[#{id}] 2>&1 &"
        logger.info rake_call
        system(rake_call)
      end

      def delete_entries_in_dictpress
        contents, emlrefs = contents_stiched_with_emlrefs

        Emldict::Dictpress::MainService.new.delete_entries_and_relations_by_tags(emlrefs)
      end

      def add_entries_in_dictpress_with_csv
        contents, emlrefs = contents_stiched_with_emlrefs

        dictpress_path = Rails.root.join("emldict", "dictpress")
        export_file_path = dictpress_path.join("export-#{id}.csv")

        File.write(
          export_file_path.to_s,
          Emldict::DictpressCsvExportService.new(
            Emldict::Rsp::ParserService.new(contents).process
          ).process
        )

        dictpress_call = "cd #{dictpress_path.to_s} && ./dictpress-repo/dictpress --import #{export_file_path.to_s} --config ./config.toml && rm #{export_file_path.to_s}"
        logger.info dictpress_call
        system(dictpress_call)
      end

      def contents_stiched_with_emlrefs_private
        contents_with_emlrefs = [contents_with_emlref]
        emlrefs = [emlref]

        prev_page_index = position - 1
        last_processed = contents
        while last_processed.starts_with?("continue_entry")
          page = work.pages.find_by(position: prev_page_index)

          last_processed = page.contents
          contents_with_emlrefs.prepend(page.contents_with_emlref)
          emlrefs.append(page.emlref)

          prev_page_index -= 1
        end

        next_page = work.pages.find_by(position: position + 1)
        while next_page.contents[0..200].include?("continue_entry")
          contents_with_emlrefs.append(next_page.contents_with_emlref)
          emlrefs.append(next_page.emlref)

          next_page = work.pages.find_by(position: next_page.position + 1)
        end

        [contents_with_emlrefs.join("\n"), emlrefs]
      end
  end
end
