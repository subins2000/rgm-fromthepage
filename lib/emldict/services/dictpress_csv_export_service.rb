# frozen_string_literal: true

require "csv"

module Emldict
  class DictpressCsvExportService
    attr_reader :word_entries

    def initialize(word_entries)
      @word_entries = word_entries
    end

    def process
      CSV.generate do |csv|
        word_entries.each do |word_entry|
          start_new_word_section(csv, word_entry)
          add_word_definitions(csv, word_entry)
          add_word_sub_entries(csv, word_entry)
        end
      end
    end

    private

      def word_entry_row(word_entry)
        [
          "-", # type
          "", # initial
          word_entry[:word], # content
          "english", # language
          "", # notes
          "", # tsvector_language
          "", # tsVector_tokens
          word_entry[:page_emlref], # tags
          word_entry[:pronunciation], # phones
          "", # definition-types
        ]
      end

      def definition_entry_row(lang, definition, word_entry)
        [
          "^", # type
          "", # initial
          definition, # content
          lang, # language
          "", # notes
          "", # tsvector_language
          "", # tsVector_tokens
          word_entry[:page_emlref], # tags
          "", # phones
          word_entry[:type], # definition-types
        ]
      end

      def start_new_word_section(csv, word_entry)
        csv << word_entry_row(word_entry)
      end

      def add_word_definitions(csv, word_entry)
        word_entry[:english_definitions].each do |english_definition|
          csv << definition_entry_row("english", english_definition, word_entry)
        end
        word_entry[:malayalam_definitions].each do |malayalam_definition|
          csv << definition_entry_row("malayalam", malayalam_definition, word_entry)
        end
      end

      def add_word_sub_entries(csv, word_entry)
        root_word = word_entry[:word]
        word_entry[:sub_entries].each do |sub_entry|
          start_new_word_section(csv, sub_entry) if sub_entry[:word] != root_word
          add_word_definitions(csv, sub_entry)
        end
      end
  end
end
