# frozen_string_literal: true

module Emldict::Dictpress
  class MainService
    def initialize
    end

    def delete_entries_and_relations_by_tags(tags = [])
      Entry.where(tags: tags).destroy_all
    end
  end
end
