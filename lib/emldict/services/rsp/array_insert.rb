# frozen_string_literal: true

module Emldict::Rsp
  module ArrayInsert
    def array_insert(key, array)
      result[key].concat(array)
    end
  end
end
