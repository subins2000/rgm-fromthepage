# frozen_string_literal: true

module Emldict::Rsp
  class ParseError < Exception
  end

  class ParserService
    attr_reader :input
    attr_accessor :results, :page_emlref

    def initialize(input)
      @input = input
      @results = []
    end

    def process
      instance_eval(input)

      results
    rescue Exception => exception
      error_causing_line = exception.backtrace[0..10].join("\n")[/^\(eval\):(\d+):/, 1].to_i

      lines_section = [
        [0, error_causing_line - 3].max,
        error_causing_line
      ]

      raise ParseError.new(
        <<~EOL
        Error in line #{error_causing_line}. Section of error:

        #{input.lines[lines_section.first..lines_section.last].each_with_index.map { |line, index| "#{lines_section.first+index}: #{line}" }.join}

        Full error:

        #{exception.to_s}

        Backtrace:
        #{exception.backtrace[0..10].join("\n")}
        EOL
      )
    end

    private

      def set_page_emlref(page_emlref)
        self.page_emlref = page_emlref
      end

      def begin_entry(&block)
        result = EntryService.new(block, page_emlref: page_emlref).process
        validate(result)
        results.append(result)
      end

      def continue_entry(&block)
        last_entry = results.last || {}
        index = [0, results.size - 1].max

        result = EntryService.new(block, previous_result: last_entry, page_emlref: page_emlref).process

        validate(results[index])

        results[index] = result
      end

      def validate(result)
        validate_entry(result)
        result[:sub_entries].map { |entry| validate_entry(entry) }
      end

      def validate_entry(entry)
        raise ParseError.new("word can't have blank value") if entry[:word].empty?
      end
  end
end
