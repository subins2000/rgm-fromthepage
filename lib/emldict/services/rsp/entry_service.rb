# frozen_string_literal: true

module Emldict::Rsp
  class EntryService
    include ArrayInsert

    RESULT_SKELETON = {
      page_emlref: "",
      word: "",
      pronunciation: "",
      type: "",
      english_definitions: [],
      malayalam_definitions: [],
      sub_entries: [],
      needs_review: ""
    }

    attr_reader :block, :main_entry_result, :page_emlref
    attr_accessor :result

    def initialize(block, previous_result: {}, main_entry_result: {}, page_emlref: "")
      @block = block
      
      # Deep clone of object
      @result = Marshal.load(Marshal.dump(
        RESULT_SKELETON.merge(page_emlref: page_emlref))
      ).merge(previous_result)

      @main_entry_result = main_entry_result
      @page_emlref = page_emlref
    end

    def process
      instance_eval(&block)
      result
    end

    private

      # Majorly we expect word to be just one, but we also accept multiple words.
      # Hence argument is plural but method name is singular
      def word(word)
        result[:word] = word
      end

      def pronunciation(pronunciation)
        result[:pronunciation] = pronunciation
      end

      def type(type)
        result[:type] = type
      end

      def english_definition(*definitions)
        array_insert(:english_definitions, definitions.reject(&:blank?))
      end

      def malayalam_definition(*definitions)
        array_insert(:malayalam_definitions, definitions.reject(&:blank?))
      end

      def needs_review(message)
        result[:needs_review] = message
      end

      def copy_main_entry_values
        self.result = Marshal.load(Marshal.dump(main_entry_result))
      end

      def copy_last_sub_entry_values
        self.result = Marshal.load(Marshal.dump(main_entry_result[:sub_entries].last))
      end

      def begin_sub_entry(&sub_entry_block)
        result[:sub_entries].append(
          EntryService.new(
            sub_entry_block,
            main_entry_result: result,
            previous_result: {
              word: result[:word],
              pronunciation: result[:pronunciation]
            },
            page_emlref: page_emlref
          ).process
        )
      end

      def continue_sub_entry(&sub_entry_block)
        last_sub_entry = result[:sub_entries].last || {}
        index = [0, result[:sub_entries].size - 1].max

        result[:sub_entries][index] = EntryService.new(
          sub_entry_block,
          previous_result: last_sub_entry,
          page_emlref: page_emlref
        ).process
      end
  end
end
