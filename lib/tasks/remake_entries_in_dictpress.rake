namespace :emldict do
  desc "Remake dictpress entries of a page"
  task :remake_entries_in_dictpress, [:page_id] => :environment do |t, args|
    page_id = args.page_id
    print "fetching page with ID=#{page_id}\n"
    page = Page.find(page_id)
    
    page.remake_entries_in_dictpress
  end
end