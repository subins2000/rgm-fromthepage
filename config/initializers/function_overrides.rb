# https://stackoverflow.com/questions/68174351/undefined-method-escape-for-urimodule

require 'uri'

module URI
  class << self
    def escape(*args)
      URI::Parser.new.escape(*args)
    end
  end
end