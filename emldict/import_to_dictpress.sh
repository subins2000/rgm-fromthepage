: "${HOST:=https://fromthepage.saparya-lab-alpha.free.gen.in}"
: "${COLLECTION_SLUG:=dictionary}"
: "${WORK_SLUG:=letter-a}"

cd dictpress
wget -O "export.csv" "$HOST/emldict/parsed/$COLLECTION_SLUG/$WORK_SLUG/export" &&
  psql -d emldictpress -c "TRUNCATE entries RESTART IDENTITY CASCADE;" &&
  ./dictpress-repo/dictpress --import ./export.csv --config ./config.toml
