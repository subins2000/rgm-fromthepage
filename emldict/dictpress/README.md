### Hot reload dictpress while editing frontend site

```bash
nodemon --watch ./site --ext '*' --signal SIGTERM --exec ./dictpress --site=./site --config=./config.toml
```
