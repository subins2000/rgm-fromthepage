# frozen_string_literal: true

class Emldict::SourcesController < Emldict::BaseController
  before_action :load_page_url

  def show
    redirect_to @url
  end

  private

    def load_page_url
      like_clause_one = "%word '#{word_param}'%"
      like_clause_two = "%word \"#{word_param}\"%"
      pages = Page.where("source_text LIKE ? OR source_text LIKE ?", like_clause_one, like_clause_two)

      render "not_found" and return if pages.size === 0
      render "results" and return if pages.size > 1

      page = pages.first
      @url = collection_display_page_path(page.collection.owner, page.collection, page.work, page)
    end

    def word_param
      params.require(:word)
    end
end
