# frozen_string_literal: true

class Emldict::ParsedController < Emldict::BaseController
  before_action :load_collection, except: :index
  before_action :load_work, except: %i[index show]
  before_action :load_page, only: %i[work_show work_export]
  before_action :load_parse_output, only: %i[work_show work_export]

  def index
    render
  end

  def show
    render
  end

  def work_show
    response_status = if @parse_exception
      :unprocessable_entity
    else
      :ok
    end
    render status: response_status
  end

  def work_export
    if @parse_exception
      render status: :unprocessable_entity, plain: @parse_exception and return
    end

    csv = Emldict::DictpressCsvExportService.new(@parse_output).process
    render status: :ok, plain: csv
  end

  def work_needs_review_show
    render
  end

  private

    def load_collection
      @collection = Collection.find_by(slug: params[:collection_slug] || params[:parsed_collection_slug])
    end

    def load_work
      @work = Work.find_by(slug: params[:work_slug])
    end

    def is_number?(obj)
      obj.to_s == obj.to_i.to_s
    end

    def load_page
      @page = if is_number?(params[:page])
        @work.pages.find_by(position: params[:page])
      else
        nil
      end
    end

    def load_parse_output
      @parse_input = if @page
        @page.contents_stiched
      else
        @work.pages.map(&:contents_with_emlref).join("\n")
      end

      begin
        @parse_output = Emldict::Rsp::ParserService.new(@parse_input).process
      rescue Exception => exception
        @parse_exception = exception
      end
    end
end
